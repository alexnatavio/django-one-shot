from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from django.http import HttpResponse
from todos.forms import TodoListForm


# Create your views here.
def show_list(request):
    task_list = TodoList.objects.all()
    context = {
        "todo_list": task_list,
    }
    return render(request, "todos/todos.html", context)


def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "item_object": detail,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    updater = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=updater)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=updater.id)
    else:
        form = TodoListForm(instance=updater)

    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    model_instance = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")
